#include <ros/ros.h>
#include <turtle_project/target_input.h>
#include <turtle_project/target.h>

#include <turtle_project/target_output.h>
#include <turtle_project/pathfinder_input.h>
#include <turtle_project/pathfinder_output.h>
#include <turtle_project/pathfinder_target_input.h>

#include <turtlesim/Pose.h>


class Controller {
public :
Controller(ros::NodeHandle node):node_(node),state_(State::WAIT){

    pathfinder_output_subscriber_  =node.subscribe("/turtle_project/pathfinder_output",10,&Controller::pathfinderOutputCallback,this);
    target_output_subscriber_=node.subscribe("/turtle_project/target_output",10,&Controller::targetOutputCallback,this);
    target_subscriber_  =node.subscribe("/turtle_project/target",10,&Controller::targetCallback,this);

    pathfinder_input_publisher_ =node.advertise<turtle_project::pathfinder_input>("/turtle_project/pathfinder_input", 10);
    target_input_publisher_=node.advertise<turtle_project::target_input>("/turtle_project/target_input",10);
    pathfinder_target_input_publisher_ =node.advertise<turtle_project::pathfinder_target_input>("/turtle_project/pathfinder_target_input", 10);

}






void process(){





    turtlesim::Pose target  =getTarget();

    turtle_project::pathfinder_output PathfinderOutput  =getPathfinderOutput();
    turtle_project::target_output TargetOutput =getTargetOutput();
   
    switch(state_){

    case State::WAIT : 

        targetInput.generate_new_target=true;
        sendTargetInput(targetInput);
    

        ROS_INFO("[Controller] WAIT");
        if(TargetOutput.new_target_generated){
        //targetInput.generate_new_target=false;
        //sendTargetInput(targetInput);

            ROS_INFO("[Controller] gooo generer une commande");
            TargetOutput.new_target_generated=false;
            state_=State::TargetGenerated;}
        break;

    case State::TargetGenerated :

            

        ROS_INFO("[Controller] generation une commande");
        pathfinderTargetInput.xtarget= target.x;
        pathfinderTargetInput.ytarget= target.y;
        sendPathfinderTargetInput(pathfinderTargetInput);
        pathfinderInput.generate_command=true;
        sendPathfinderInput(pathfinderInput);
        
        if(PathfinderOutput.command_generated)
        {PathfinderOutput.command_generated=false;
        ROS_INFO("[Controller] Commande générée");
        state_=State::WAIT;}
        break;

    }// end switch


    } // end process 
/*
    turtlesim::Pose target  =getTarget();

    turtle_project::pathfinder_output PathfinderOutput  =getPathfinderOutput();
    turtle_project::target_output TargetOutput =getTargetOutput();
   
    switch(state_){

    case State::WAIT : 

        targetInput.generate_new_target=true;
        sendTargetInput(targetInput);
    

        ROS_INFO("[Controller] WAIT");
        if(TargetOutput.new_target_generated){
        ROS_INFO("[Controller] gooo generer une commande");

            TargetOutput.new_target_generated=false;
        state_=State::WaitForTargetGenerated;}
        break;

    case State::WaitForTargetGenerated :

            

       // if (TargetOutput.new_target_generated){
        ROS_INFO("[Controller] go generer une commande");
        //TargetOutput.new_target_generated=false; 
        pathfinderInput.xtarget= target.x;
        pathfinderInput.ytarget= target.y;
        pathfinderInput.generate_command=true;
        sendPathfinderInput(pathfinderInput);
        state_=State::WaitForCommandGenerated;
        //}
        break;

    case State::WaitForCommandGenerated :
    
    if(PathfinderOutput.command_generated)
    {PathfinderOutput.command_generated=false;
    ROS_INFO("[Controller] Commande générée");
    state_=State::WAIT;
    }
    break;




    }// end switch


    } // end process 

*/



private :

enum class State {WAIT,TargetGenerated,WaitForCommandGenerated};
State state_;

void targetOutputCallback(const typename turtle_project::target_output::ConstPtr& message){
    targetOutput=*message;
}

void pathfinderOutputCallback(const typename turtle_project::pathfinder_output::ConstPtr& message){
    pathfinderOutput=*message;

}

void targetCallback(const typename turtlesim::Pose::ConstPtr& message){
    target_coord=*message;

}

void sendTargetInput(turtle_project::target_input target_input_message) {
target_input_publisher_.publish(target_input_message);
}


void sendPathfinderInput(turtle_project::pathfinder_input pathfinder_input_message) {
pathfinder_input_publisher_.publish(pathfinder_input_message);
}
void sendPathfinderTargetInput(turtle_project::pathfinder_target_input pathfinder_target_input_message) {
pathfinder_target_input_publisher_.publish(pathfinder_target_input_message);
}


turtle_project::target_output getTargetOutput() {
    return targetOutput;
};
turtle_project::pathfinder_output getPathfinderOutput() {
    return pathfinderOutput;
};

turtlesim::Pose getTarget() {
        return target_coord;
    };



ros::NodeHandle node_;

ros::Subscriber pathfinder_output_subscriber_;
ros::Subscriber target_output_subscriber_;
ros::Subscriber target_subscriber_;


ros::Publisher pathfinder_input_publisher_;
ros::Publisher pathfinder_target_input_publisher_;

ros::Publisher target_input_publisher_;


turtle_project::target_output targetOutput;
turtle_project::pathfinder_output pathfinderOutput;
turtle_project::target_input targetInput;
turtle_project::pathfinder_input pathfinderInput;
turtle_project::pathfinder_target_input pathfinderTargetInput;

turtlesim::Pose target_coord;






};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controller");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controller controller(node);

    while (ros::ok()) {
        controller.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
