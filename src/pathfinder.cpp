#include <ros/ros.h>
#include <turtle_project/target.h>
#include <turtle_project/target_input.h>
#include <turtle_project/pathfinder_target_input.h>
#include <turtle_project/target_output.h>
#include <turtle_project/pathfinder_input.h>
#include <turtle_project/pathfinder_output.h>
#include <turtlesim/Pose.h>
#include <geometry_msgs/Twist.h>


class Pathfinder {
public :
Pathfinder(ros::NodeHandle node):node_(node),state_(State::WAIT){

    pose_subscriber_    =node.subscribe("/turtle1/pose", 10, &Pathfinder::poseCallback,this);
    //target_subscriber_  =node.subscribe("/turtle_project/target",10,&Pathfinder::targetCallback,this);
    pathfinder_input_subscriber_=node.subscribe("/turtle_project/pathfinder_input",10,&Pathfinder::pathfinderInputCallback,this);
    pathfinder_target_input_subscriber_=node.subscribe("/turtle_project/pathfinder_target_input",10,&Pathfinder::pathfinderTargetInputCallback,this);

    pathfinder_output_=node.advertise<turtle_project::pathfinder_output>("/turtle_project/pathfinder_output", 10);
    velocity_publisher_ =node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 10);
    //target_input_publisher_=node.advertise<turtle_project::target_input>("/turtle_project/target_input",10);
}






void process(){

    //turtlesim::Pose target  =getTarget();
    turtlesim::Pose pose    =getPose();
    turtle_project::pathfinder_input pathfinder_input= getPathfinderInput();
    turtle_project::pathfinder_target_input pathfinder_target_input= getPathfinderTargetInput();

   


    switch(state_){

    case State::WAIT : 
         
        velocity_cmd.angular.z=0.0;
        velocity_cmd.linear.x=0.0;
        sendVelocity(velocity_cmd);


        ROS_INFO("[Pathfinder] WAIT");
        
        if (pathfinderInput.generate_command){
        ROS_INFO("[Pathfinder] go generer une commande");
        pathfinderInput.generate_command=false;       

        state_=State::GeneratingCommand;}
        break;

    case State::GeneratingCommand :
    ROS_INFO("[Pathfinder] generation d'une commande");
    
    auto turtle_x= pose.x;
    auto turtle_y= pose.y;
    auto turtle_theta= pose.theta;
    auto target_x= pathfinder_target_input.xtarget;
    auto target_y= pathfinder_target_input.ytarget;
    //auto target_theta= target.theta;


    float distance;
    distance = sqrt(std::pow((target_x - turtle_x),2) + std::pow((target_y - turtle_y),2));
    
    while(distance>0.1){
    float vitesse_x = k_p_vit * distance;
    float theta_dot=k_p_ang * atan2(target_x - turtle_x, target_y - turtle_y) - turtle_theta;

    velocity_cmd.angular.z=theta_dot;
    velocity_cmd.linear.x=vitesse_x;
    sendVelocity(velocity_cmd);
    distance = sqrt(std::pow((target_x - turtle_x),2) + std::pow((target_y - turtle_y),2));
    }
    pathfinderOutput.command_generated=true;
    sendPathfinderOutput(pathfinderOutput);

    state_=State::WAIT;
    break;
    
    }// end switch


    } // end process 




private :

enum class State {WAIT,GeneratingCommand};
State state_;


void poseCallback(const typename turtlesim::Pose::ConstPtr& message){
    pose_turtle=*message;

}

/*void targetCallback(const typename turtlesim::Pose::ConstPtr& message){
    target_coord=*message;

}*/

void pathfinderInputCallback(const typename turtle_project::pathfinder_input::ConstPtr& message){
    pathfinderInput=*message;
}

void pathfinderTargetInputCallback(const typename turtle_project::pathfinder_target_input::ConstPtr& message){
    pathfinderTargetInput=*message;
}


void sendVelocity(geometry_msgs::Twist velocity_message) {
velocity_publisher_.publish(velocity_message);
}

void sendPathfinderOutput(turtle_project::pathfinder_output pathfinder_output_message) {
pathfinder_output_.publish(pathfinder_output_message);
}



    turtlesim::Pose getPose() {
        return pose_turtle;
    };
    /*turtlesim::Pose getTarget() {
        return target_coord;
    };*/
    
    turtle_project::pathfinder_input getPathfinderInput(){
        return pathfinderInput;
    };
    turtle_project::pathfinder_target_input getPathfinderTargetInput(){
        return pathfinderTargetInput;
    };


ros::NodeHandle node_;

ros::Subscriber pose_subscriber_;
//ros::Subscriber target_subscriber_;
ros::Subscriber pathfinder_input_subscriber_;
ros::Subscriber pathfinder_target_input_subscriber_;


ros::Publisher velocity_publisher_;
ros::Publisher pathfinder_output_;

turtlesim::Pose pose_turtle;
//turtlesim::Pose target_coord;

geometry_msgs::Twist velocity_cmd;

turtle_project::pathfinder_output pathfinderOutput;
turtle_project::pathfinder_input pathfinderInput;
turtle_project::pathfinder_target_input pathfinderTargetInput;


float k_p_vit = 0.2;
float k_p_ang = 0.3;

};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "pathfinder");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Pathfinder pathfinder(node);

    while (ros::ok()) {
        pathfinder.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
