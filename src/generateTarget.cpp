#include <ros/ros.h>
#include <turtlesim/Pose.h>
#include <stdlib.h>
#include <turtle_project/target.h>
#include <turtle_project/target_input.h>
#include <turtle_project/target_output.h>
#include <iostream>

class GenerateTarget
{
public:
    GenerateTarget(ros::NodeHandle node):node_(node),state_(State::WAIT)
    { /* ---- creation publisher pour la pose cible ---- */
        target_publisher_ = node.advertise<turtlesim::Pose>("/turtle_project/target", 10);
        target_output_publisher_ = node.advertise<turtle_project::target_output>("/turtle_project/target_output", 10);


        target_input_subscriber_ = node.subscribe("/turtle_project/target_input", 10, &GenerateTarget::target_inputCallback,this);

    
    }

    void process()
    {  

       turtle_project::target_input targetInput = getTarget_input();


       switch(state_){

           case State::WAIT :
                           ROS_INFO("[target] WAIT 1");

                if(targetInput.generate_new_target){
                targetInput.generate_new_target=false;
                ROS_INFO("[target] WAIT 2");
                state_=State::GeneratingTarget;
                }
                break;

            case State::GeneratingTarget :
            //targetInput.generate_new_target=false;

                ROS_INFO("[target] generation d'une target");

                new_target.x = (rand() % 100) / 10;
                new_target.y = (rand() % 100) / 10;
                sendTarget(new_target);
                target_output.new_target_generated=true;
                sendTargetOutput(target_output);
                state_=State::WAIT;
                break;
        }
    }
    

// ---------------------------------------------------------------- //


private:   

enum class State{WAIT,GeneratingTarget};
State state_;


// Publish

void sendTarget(turtlesim::Pose target_message) {
target_publisher_.publish(target_message);
}

void sendTargetOutput(turtle_project::target_output target_output_message) {
target_output_publisher_.publish(target_output_message);
}


// Subscribe

void target_inputCallback(const typename turtle_project::target_input::ConstPtr& message){
    target_input=*message;

}

turtle_project::target_input getTarget_input() {
        return target_input;
    };

// ---------------------------------------------------------------- //

ros::NodeHandle node_;
turtle_project::target_input target_input;
turtlesim::Pose new_target;
turtle_project::target_output target_output;

ros::Subscriber target_input_subscriber_;
ros::Publisher target_publisher_;
ros::Publisher target_output_publisher_;

};



/*
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
*/




int main(int argc, char *argv[])
{
    ros::init(argc, argv, "generateTarget");
    ros::NodeHandle node;
    ros::Rate loop_rate(50);

    GenerateTarget Target(node);

    while (ros::ok())
    {
        Target.process();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
